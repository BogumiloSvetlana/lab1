package server.model;

import java.util.Random;

public class TimestampGen {

    public long generateTimestamp() {
        return System.currentTimeMillis() / 1000;
    }

    public long generateNonce(long timestamp) {
        return timestamp + RANDOM.nextInt();
    }

    static final Random RANDOM = new Random();

}