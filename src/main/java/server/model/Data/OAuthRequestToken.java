package server.model.Data;


public class OAuthRequestToken extends OAuthToken {

    private String verifier;

    public OAuthRequestToken(OAuthToken token, String verifier) {
        super(token.getValue(), token.getSecret());
        this.verifier = verifier;
    }

    public OAuthRequestToken(String value, String secret, String verifier) {
        super(value, secret);
        this.verifier = verifier;
    }

    public String getVerifier() {
        return verifier;
    }

    public void setVerifier(String verifier) {
        this.verifier = verifier;
    }
}
