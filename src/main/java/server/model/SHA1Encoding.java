package server.model;

import org.springframework.http.HttpMethod;
import org.springframework.util.Assert;
import org.springframework.util.MultiValueMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class SHA1Encoding {

    private final String HMAC_SHA1_MAC_NAME = "HmacSHA1";
    private final String UTF8_CHARSET_NAME = "UTF-8";
    private final BitSet UNRESERVED;

    public SHA1Encoding() {
        BitSet alpha = new BitSet(256);
        for (int i = 'a'; i <= 'z'; i++) {
            alpha.set(i);
        }
        for (int i = 'A'; i <= 'Z'; i++) {
            alpha.set(i);
        }
        BitSet digit = new BitSet(256);
        for (int i = '0'; i <= '9'; i++) {
            digit.set(i);
        }
        BitSet unreserved = new BitSet(256);
        unreserved.or(alpha);
        unreserved.or(digit);
        unreserved.set('-');
        unreserved.set('.');
        unreserved.set('_');
        unreserved.set('~');
        UNRESERVED = unreserved;
    }

    public String formEncode(String data) {
        try {
            return URLEncoder.encode(data, "UTF-8");
        }
        catch (UnsupportedEncodingException ex) {
            throw new IllegalStateException(ex);
        }
    }

    public String oauthEncode(String param) {
        byte[] bytes = new byte[0];
        try {
            bytes = encode(param.getBytes(UTF8_CHARSET_NAME), UNRESERVED);
            return new String(bytes, "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return new String("");
    }

    public String calculateSignature(String baseString, String consumerSecret, String tokenSecret) {
        String key = oauthEncode(consumerSecret) + "&" + (tokenSecret != null ? oauthEncode(tokenSecret) : "");
        return sign(baseString, key);
    }

    public String buildBaseString(HttpMethod method, String targetUrl, MultiValueMap<String, String> collectedParameters) {
        String builder = method.name() + '&' + oauthEncode(targetUrl) + '&' +
                oauthEncode(normalizeParameters(collectedParameters));
        return builder;
    }

    private byte[] encode(byte[] source, BitSet notEncoded) {
        Assert.notNull(source, "'source' must not be null");
        ByteArrayOutputStream bos = new ByteArrayOutputStream(source.length * 2);
        for (byte aSource : source) {
            int b = aSource;
            if (b < 0) {
                b += 256;
            }
            if (notEncoded.get(b)) {
                bos.write(b);
            } else {
                bos.write('%');
                char hex1 = Character.toUpperCase(Character.forDigit((b >> 4) & 0xF, 16));
                char hex2 = Character.toUpperCase(Character.forDigit(b & 0xF, 16));
                bos.write(hex1);
                bos.write(hex2);
            }
        }
        return bos.toByteArray();
    }

    private String normalizeParameters(MultiValueMap<String, String> collectedParameters) {
        MultiValueMap<String, String> sortedEncodedParameters = new TreeMultiValueMap<>();
        for (Map.Entry<String, List<String>> entry : collectedParameters.entrySet()) {
            String collectedName = entry.getKey();
            List<String> collectedValues = entry.getValue();
            List<String> encodedValues = new ArrayList<>(collectedValues.size());
            sortedEncodedParameters.put(oauthEncode(collectedName), encodedValues);
            for (String value : collectedValues) {
                encodedValues.add(value != null ? oauthEncode(value) : "");
            }
            Collections.sort(encodedValues);
        }
        StringBuilder paramsBuilder = new StringBuilder();
        for (Iterator<Map.Entry<String, List<String>>> entryIt = sortedEncodedParameters.entrySet().iterator(); entryIt.hasNext();) {
            Map.Entry<String, List<String>> entry = entryIt.next();
            String name = entry.getKey();
            List<String> values = entry.getValue();
            for (Iterator<String> valueIt = values.iterator(); valueIt.hasNext();) {
                String value = valueIt.next();
                paramsBuilder.append(name).append('=').append(value);
                if (valueIt.hasNext()) {
                    paramsBuilder.append("&");
                }
            }
            if (entryIt.hasNext()) {
                paramsBuilder.append("&");
            }
        }
        return paramsBuilder.toString();
    }

    private String sign(String signatureBaseString, String key) {
        try {
            Mac mac = Mac.getInstance(HMAC_SHA1_MAC_NAME);
            SecretKeySpec spec = new SecretKeySpec(key.getBytes(), HMAC_SHA1_MAC_NAME);
            mac.init(spec);
            byte[] text = signatureBaseString.getBytes(UTF8_CHARSET_NAME);
            byte[] signatureBytes = mac.doFinal(text);
            signatureBytes = Base64.getEncoder().encode(signatureBytes);
            String signature = new String(signatureBytes, UTF8_CHARSET_NAME);
            return signature;
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e);
        } catch (InvalidKeyException e) {
            throw new IllegalStateException(e);
        } catch (UnsupportedEncodingException shouldntHappen) {
            throw new IllegalStateException(shouldntHappen);
        }
    }
}
