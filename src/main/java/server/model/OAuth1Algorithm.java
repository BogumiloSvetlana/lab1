package server.model;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import server.model.Data.OAuthRequestToken;
import server.model.Data.OAuthToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class OAuth1Algorithm {

    static private Logger OAuth1Log = Logger.getLogger(OAuth1Algorithm.class.getName());
    private final RestTemplate restTemplate = createRestTemplate();
    private TimestampGen timestampGenerator = new TimestampGen();
    private final String HMAC_SHA1_SIGNATURE_NAME = "HMAC-SHA1";
    private SHA1Encoding EncodeClass = new SHA1Encoding();

    public String BuildAuthenticateUrl(String AuthorizeUrl, OAuthToken RequestToken)
    {
        String Params = "oauth_token=" + EncodeClass.formEncode(RequestToken.getValue());
        OAuth1Log.info("Authorize url created, " + Params);
        return AuthorizeUrl + '?' + Params;
    }

    public OAuthToken FetchRequestToken(String Key, String Secret, String callbackUrl, String RequestUrl) {
        URI RequestUrlTransformed = StringToURI(RequestUrl);
        Map<String, String> oauthParameters = new HashMap<>(1, 1);
        oauthParameters.put("oauth_callback", callbackUrl);
        OAuth1Log.info("Request token requested: key="+Key+", secret="+Secret);
        OAuthToken RequestToken = exchangeForToken(Key, Secret, RequestUrlTransformed, oauthParameters, null);
        OAuth1Log.info("Request token accepted: value="+RequestToken.getValue()+", secret="+RequestToken.getSecret());
        return RequestToken;

    }

    public OAuthToken GetAcessToken(String Key, String Secret, OAuthToken RequestToken, String AccessUrl, String Verifier) {
        URI AccessUrlTransformed = StringToURI(AccessUrl);
        OAuthRequestToken AutorToken = new OAuthRequestToken(RequestToken,Verifier);
        Map<String, String> tokenParameters = new HashMap<>(2, 1);
        tokenParameters.put("oauth_token", AutorToken.getValue());
        tokenParameters.put("oauth_verifier", AutorToken.getVerifier());
        OAuth1Log.info("Aceess token requested: key="+Key+", secret="+Secret+ ", value="+RequestToken+", verifier="+Verifier);
        OAuthToken AccessToken =  exchangeForToken(Key, Secret, AccessUrlTransformed, tokenParameters, AutorToken.getSecret());
        OAuth1Log.info("Access token accepted: value="+AccessToken.getValue()+", secret="+AccessToken.getSecret());
        return AccessToken;
    }

    public JSONObject GetRequestObject(String adress, String consumerKey, String Secret, String token, String tokenSecret)
    {

        try {
            OAuth1Log.info("Get Object requested: url="+adress+", key="+consumerKey+", secret="+Secret+ ", value="+token+", secretToken="+tokenSecret);
            OAuthConsumer oAuthConsumer = new CommonsHttpOAuthConsumer(consumerKey,Secret);
            oAuthConsumer.setTokenWithSecret(token, tokenSecret);
            HttpGet httpGet = new HttpGet(adress);
            oAuthConsumer.sign(httpGet);
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse httpResponse = httpClient.execute(httpGet);
            org.apache.http.HttpEntity httpEntity = httpResponse.getEntity();
            InputStream inputStream = httpEntity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
            String answer = reader.readLine();
            reader.close();
            OAuth1Log.info("Object received: "+answer);
            JSONObject root = new JSONObject(answer);
            return root;
    } catch (OAuthMessageSignerException e) {
        e.printStackTrace();
    } catch (OAuthExpectationFailedException e) {
        e.printStackTrace();
    } catch (OAuthCommunicationException e) {
        e.printStackTrace();
    } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new JSONObject();
    }

    public JSONArray GetRequestArray(String adress, String consumerKey, String Secret, String token, String tokenSecret)
    {

        try {
            OAuth1Log.info("Get Array requested: url="+adress+", key="+consumerKey+", secret="+Secret+ ", value="+token+", secretToken="+tokenSecret);
            OAuthConsumer oAuthConsumer = new CommonsHttpOAuthConsumer(consumerKey,Secret);
            oAuthConsumer.setTokenWithSecret(token, tokenSecret);
            HttpGet httpGet = new HttpGet(adress);
            oAuthConsumer.sign(httpGet);
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse httpResponse = httpClient.execute(httpGet);
            org.apache.http.HttpEntity httpEntity = httpResponse.getEntity();
            InputStream inputStream = httpEntity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
            String answer = reader.readLine();
            reader.close();
            OAuth1Log.info("Array received: "+answer);
            JSONArray root = new JSONArray(answer);
            return root;
        } catch (OAuthMessageSignerException e) {
            e.printStackTrace();
        } catch (OAuthExpectationFailedException e) {
            e.printStackTrace();
        } catch (OAuthCommunicationException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new JSONArray();
    }


    public String PostRequest(String adress, String consumerKey, String Secret, String token, String tokenSecret)
    {
        try {
            OAuth1Log.info("Post: url="+adress+", key="+consumerKey+", secret="+Secret+ ", token="+token+", secretToken="+tokenSecret);
            OAuthConsumer oAuthConsumer = new CommonsHttpOAuthConsumer(consumerKey,Secret);
            oAuthConsumer.setTokenWithSecret(token, tokenSecret);
            HttpPost httpPost = new HttpPost(adress);
            oAuthConsumer.sign(httpPost);
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse httpResponse = httpClient.execute(httpPost);
            Integer Status = httpResponse.getStatusLine().getStatusCode();

            if (Status == 200) {
                OAuth1Log.info("Posted: Status code = " + Status.toString());
                return "Tweet posted. Status code = " + Status.toString();
            }
            OAuth1Log.info("Not posted: Status code = " + Status.toString());
            return "Tweet not posted. Error code = "+ Status.toString();
        } catch (OAuthMessageSignerException e) {
            e.printStackTrace();
        } catch (OAuthExpectationFailedException e) {
            e.printStackTrace();
        } catch (OAuthCommunicationException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "fail";
    }

    // ------------------------------- private inner methods-------------------------------------------------------------
    private OAuthToken exchangeForToken(String Key, String Secret, URI tokenUrl, Map<String, String> tokenParameters, String tokenSecret) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", buildHeaderValue(Key, Secret, tokenUrl, tokenParameters, tokenSecret));
        ResponseEntity<MultiValueMap> response = restTemplate.exchange(tokenUrl, HttpMethod.POST, new HttpEntity<MultiValueMap<String, String>>(null, headers), MultiValueMap.class);
        MultiValueMap<String, String> body = response.getBody();
        return new OAuthToken(body.getFirst("oauth_token"), body.getFirst("oauth_token_secret"));
    }

    private String buildHeaderValue(String Key, String Secret, URI tokenUrl, Map<String, String> tokenParameters, String tokenSecret) {
        Map<String, String> oauthParameters = SetOAuthParameters(Key,tokenParameters);

        StringBuilder header = new StringBuilder();
        header.append("OAuth ");
        for (Map.Entry<String, String> entry : oauthParameters.entrySet()) {
            header.append(EncodeClass.oauthEncode(entry.getKey())).append("=\"").append(EncodeClass.oauthEncode(entry.getValue())).append("\", ");
        }
        MultiValueMap<String, String> collectedParameters = new LinkedMultiValueMap<>((int) ((oauthParameters.size()) / .75 + 1));
        collectedParameters.setAll(oauthParameters);
        String baseString = EncodeClass.buildBaseString(HttpMethod.POST, getBaseStringUri(tokenUrl), collectedParameters);
        String signature = EncodeClass.calculateSignature(baseString, Secret, tokenSecret);
        header.append(EncodeClass.oauthEncode("oauth_signature")).append("=\"").append(EncodeClass.oauthEncode(signature)).append("\"");
        return header.toString();
    }

    private RestTemplate createRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> converters = new ArrayList<>(1);
        converters.add(new FormHttpMessageConverter() {
            public boolean canRead(Class<?> clazz, MediaType mediaType) {
                return MultiValueMap.class.isAssignableFrom(clazz);
            }
        });
        restTemplate.setMessageConverters(converters);
        return restTemplate;
    }

    private Map<String, String> SetOAuthParameters(String consumerKey,Map<String, String> tokenParameters) {
        Map<String, String> oauthParameters = new HashMap<>();
        oauthParameters.put("oauth_consumer_key", consumerKey);
        oauthParameters.put("oauth_signature_method", HMAC_SHA1_SIGNATURE_NAME);
        long timestamp = timestampGenerator.generateTimestamp();
        oauthParameters.put("oauth_timestamp", Long.toString(timestamp));
        oauthParameters.put("oauth_nonce", Long.toString(timestampGenerator.generateNonce(timestamp)));
        oauthParameters.put("oauth_version", "1.0");
        oauthParameters.putAll(tokenParameters);
        return oauthParameters;
    }

    private URI StringToURI(String address) {
        return UriComponentsBuilder.fromUriString(address).build().toUri();
    }

    private String getBaseStringUri(URI uri) {
        try {
            return new URI(uri.getScheme(), null, uri.getHost(), getPort(uri), uri.getPath(), null, null).toString();
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private int getPort(URI uri) {
        if (uri.getScheme().equals("http") && uri.getPort() == 80 || uri.getScheme().equals("https") && uri.getPort() == 443) {
            return -1;
        } else {
            return uri.getPort();
        }
    }


    private String buildHeaderValue2(String Key, String token, String Secret, URI tokenUrl, Map<String, String> additionalParameters, String tokenSecret) {

        Map<String, String> oauthParameters = new HashMap<>();

        oauthParameters.put("oauth_consumer_key", Key);
        oauthParameters.put("oauth_token",token);
        oauthParameters.put("oauth_signature_method", HMAC_SHA1_SIGNATURE_NAME);
        oauthParameters.put("oauth_version", "1.0");
        long timestamp = timestampGenerator.generateTimestamp();
        oauthParameters.put("oauth_nonce", Long.toString(timestampGenerator.generateNonce(timestamp)));
        oauthParameters.put("oauth_timestamp", Long.toString(timestamp));
        oauthParameters.putAll(additionalParameters);


        StringBuilder header = new StringBuilder();
        for (Map.Entry<String, String> entry : oauthParameters.entrySet()) {
            header.append(EncodeClass.oauthEncode(entry.getKey())).append("=\"").append(EncodeClass.oauthEncode(entry.getValue())).append("\"&");
        }
        for (Map.Entry<String, String> entry : additionalParameters.entrySet()) {
            header.append(EncodeClass.oauthEncode(entry.getKey())).append("=").append(EncodeClass.oauthEncode(entry.getValue())).append("&");
        }


        MultiValueMap<String, String> collectedParameters = new LinkedMultiValueMap<>((int) ((oauthParameters.size()+ additionalParameters.size()) / .75 + 1));
        collectedParameters.setAll(oauthParameters);
        for (Map.Entry<String, String> entry : additionalParameters.entrySet()) {
            collectedParameters.add(entry.getKey(),entry.getValue());
        }
        String baseString = EncodeClass.buildBaseString(HttpMethod.POST, getBaseStringUri(tokenUrl), collectedParameters);
        String signature = EncodeClass.calculateSignature(baseString, Secret, tokenSecret);
        header.append(EncodeClass.oauthEncode("oauth_signature")).append("=\"").append(EncodeClass.oauthEncode(signature)).append("\"");
        return header.toString();
    }

}
