package server.service;

import org.json.JSONObject;

public interface OAuthTwitterInterface {

    public String GetRequestToken();

    public String GetAcessToken(String Verifier);

    public String PostTweet(String adress, String message);

    public JSONObject GetObject(String adress);

}
