package server.service;

import org.json.JSONObject;
import org.springframework.stereotype.Service;
import server.model.Data.OAuthConnectionData;
import server.model.Data.OAuthToken;
import server.model.OAuth1Algorithm;

@Service
public class OAuth1Twitter implements OAuthTwitterInterface{

    private OAuth1Algorithm algorithm = new OAuth1Algorithm();
    private OAuthConnectionData TwitterData = new OAuthConnectionData("6BHakk0c6FzmtfVXRxYgDlaqF","f8bFGr6oWUHlpoKH2NPITzvATayViQjQdIewRn1WWSrtLJXYUD","http://test.local/twitter","https://api.twitter.com/oauth/request_token","https://api.twitter.com/oauth/authorize","https://api.twitter.com/oauth/access_token");
    private OAuthToken AccessToken;


    public String GetRequestToken()
    {
        OAuthToken Token = algorithm.FetchRequestToken(TwitterData.getKey(), TwitterData.getSecret(), TwitterData.getCallbackURL(),TwitterData.getRequestUrl());
        TwitterData.setRequestToken(Token);
        return algorithm.BuildAuthenticateUrl(TwitterData.getAuthorizeUrl(),TwitterData.getRequestToken());
    }

    public String GetAcessToken(String nVerifier)
    {
        AccessToken = algorithm.GetAcessToken(TwitterData.getKey(), TwitterData.getSecret(), TwitterData.getRequestToken(), TwitterData.getAccessUrl(), nVerifier);
        return "oauth_token_secret="+AccessToken.getSecret()+"&oauth_token="+AccessToken.getValue();
    }

    public String PostTweet(String adress, String message)
    {
        return algorithm.PostRequest(adress + "?status="+message, TwitterData.getKey(), TwitterData.getSecret(), AccessToken.getValue(), AccessToken.getSecret());
    }

    public JSONObject GetObject(String adress)
    {
        return algorithm.GetRequestObject(adress, TwitterData.getKey(), TwitterData.getSecret(), AccessToken.getValue(), AccessToken.getSecret());
    }

}
